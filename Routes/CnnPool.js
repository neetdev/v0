var mysql = require('mysql');

// Constructor for DB connection pool
var CnnPool = function() {
   var poolCfg = require('./connection.json');

   poolCfg.connectionLimit = CnnPool.PoolSize;
   this.pool = mysql.createPool(poolCfg);
};

CnnPool.PoolSize = 10;

// The one (and probably only) CnnPool object needed for the app
CnnPool.singleton = new CnnPool();

// Conventional getConnection, drawing from the pool
CnnPool.prototype.getConnection = function(cb) {
   this.pool.getConnection(cb);
};

// Router function for use in auto-creating CnnPool for a request
CnnPool.router = function(req, res, next) {
   console.log("Getting connection");
   CnnPool.singleton.getConnection(function(err, cnn) {
      if (err) {
         res.status(500).json('Failed to get connection ' + err);
         console.log("Failed to get connection");
      } else {
         console.log("Connection acquired");
         cnn.chkQry = function(qry, prms, cb) {
            // Run real qry, checking for error
            this.query(qry, prms, function(err, result, fields) {
               console.log("Query running");
               if (err){
                  console.log(err);
                  res.status(500).json('Failed query ' + qry);
                  console.log("Query failed");
               }
               cb(err, result, fields);
            });
         };
         req.cnn = cnn;
         next();
      }
   });
};


module.exports = CnnPool;
