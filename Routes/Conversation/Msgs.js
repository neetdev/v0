var Express = require('express');
var Tags = require('../Validator.js').Tags;
var router = Express.Router({caseSensitive: true});
var async = require('async');

router.baseURL = '/Msgs';

router.get('/:msgId', function(req, res){
	var vld = req.validator;
	var cnn = req.cnn;
	var msgId = req.params.msgId;
	if(vld.checkPrsOK()){
      // query is incorrect
		req.cnn.chkQry('select whenMade, email, content, numLikes, replies from Message where id = ?', [msgId],
			function(err, msg){
				if(!err)
					res.status(200).json(msg);
				cnn.release();
			});
	} else {
      cnn.release();
      //res.status(401).end(); // not logged in
   }
});

router.get('/:msgId/Replies', function(req, res){
	var vld = req.validator;
	var cnn = req.cnn;
	var msgId = req.params.msgId;

});

router.post('/:msgId/Replies', function(req, res){
	var vld = req.validator;
	var cnn = req.cnn;
	var body = req.body;
	var msgId = req.params.msgId;

});

router.get('/:msgId/Likes', function(req, res){
	var vld = req.validator;
	var cnn = req.cnn;
	var msgId = req.params.msgId;

});

router.post('/:msgId/Likes', function(req, res){
	var vld = req.validator;
	var cnn = req.cnn;
	var body = req.body;
	var msgId = req.params.msgId;

});

module.exports = router;
